springboot - k8s

1. 编译
```bash
mvn clean package -Dmaven.test.skip=true
```

2. 构建镜像
```bash
cd docker
docker build -t k8s:0.1.0
```

3. 部署
```bash
kubectl create -f k8s.yml
```

4. 关闭清理
```bash
kubectl delete -f k8s.yml
```


更多
