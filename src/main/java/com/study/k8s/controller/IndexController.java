package com.study.k8s.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
public class IndexController {
    private static final String uuid = UUID.randomUUID().toString();

    @Value("${api.config:java}")
    private String config;

    @GetMapping({"", "/"})
    public Object index() {
        return config + ":" + uuid;
    }

}
